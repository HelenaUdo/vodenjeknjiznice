#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include "Header.h"
#include <windows.h>

void izbornik (KNJIGA* book) {

	int izbor;

	printf("\n|=======================|");
	printf("\n|----{  KNJIZNICA  }----|");
	printf("\n|=======================|\n");
	printf("\n1. Unesite novu knjigu\n2. Pretrazivanje knjige prema imenu\n3. Ispis svih knjiga iz arhive\n4. Brisanje arhive\n5. Izlaz");
	printf("\n\nOdaberite jednu od ponudenih opcija: ");
	scanf("%d", &izbor);

	switch (izbor) {
	case 1: 
		system("cls");
		novaKnjiga();
		izbornik(book);
		break;

	case 2:
		system("cls");
		pretraga();
		izbornik(book);
		break;

	case 3:
		system("cls");
		ispis();
		izbornik(book);
		break;

	case 4:
		system("cls");
		brisanje();
		izbornik(book);
		break;

	case 5:
		izlaz();
		break;

	default:
		printf("\nPonovno odaberite broj iz intervala od 1 do 5!\n\n");
		izbornik(book);
		break;

	}

	return 0;

}


void novaKnjiga(void) {

	KNJIGA* pokNaKnj;
	pokNaKnj = (KNJIGA*)calloc(1, sizeof(KNJIGA));

	if (pokNaKnj == 1) {
		perror("Dogodila se pogreska pri alokaciji memorije!");
	}

	FILE* fp = NULL;
	fp = fopen("binarna.bin", "ab");

	if (fp == NULL) {
		perror("Pogreska pri otvaranju dokumenta!");
	}

	else {
		printf("====={ NOVA KNJIGA }=====");

		printf("\nIme knjige: ");
		//scanf("%[^\n]s", &pokNaKnj->ime_knjige);
		getchar();
		fgets(pokNaKnj->ime_knjige, 41, stdin);

		printf("\nIme autora knjige: ");
		//scanf("%[^\n]s", &pokNaKnj->autor);
		fgets(pokNaKnj->autor, 41, stdin);

		printf("\nGodina izdanja: ");
		scanf("%d", &pokNaKnj->godina_izdanja);

		printf("\nUnesite ISBN knjige: ");
		scanf("%ld", &pokNaKnj->ISBN);

		printf("\nUkupan broj novih knjiga: ");
		scanf("%d", &pokNaKnj->broj_knjiga);

		system("cls");
		printf("\n");

		printf("Ime knjige: %s\nIme autora: %s\nGodina izdanja: %d\n\nISBN knjige: %ld\n\nBroj knjiga: %d\n", pokNaKnj->ime_knjige, pokNaKnj->autor, pokNaKnj->godina_izdanja, pokNaKnj->ISBN, pokNaKnj->broj_knjiga);

	}
	fwrite(pokNaKnj, sizeof(KNJIGA), 1, fp);
	fclose(fp);
	free(pokNaKnj);

	brojac();

	return 0;

}


void ispis(void) {

	provjera();
	FILE* fp = NULL;
	fp = fopen("binarna.bin", "rb");
	KNJIGA* pokNaKnj = NULL;

	int a = 0;
	int i;

	if (fp == NULL) {
		perror("Pogreska pri otvaranju dokumenta!");
	}

	else {
		fread(&a, sizeof(int), 1, fp);
		if (a == 0) {
			printf("Dokument je prazan\n\n");
		}
		pokNaKnj = (KNJIGA*)calloc(a, sizeof(KNJIGA));

		if (pokNaKnj == 1) {
			perror("Greska");
		}
		fseek(fp, sizeof(int), SEEK_SET);
		fread(pokNaKnj, sizeof(KNJIGA), a, fp);

		int odg;

		printf("Zelite li ispisati knjige od najstarije prema najmladoj? ");
		scanf("%d", &odg);

		if (odg == 1) {

			sortiranje(pokNaKnj, a);

			for (i = 0; i < a; i++) {
				printf("\n--{ %d. Knjiga }--\n\nIme knjige: %s\nIme autora: %s\nGodina izdanja: %d\n\nISBN knjige: %ld\n\nBroj knjiga: %d\n\n", i + 1, (pokNaKnj + i)->ime_knjige, (pokNaKnj + i)->autor, (pokNaKnj + i)->godina_izdanja, (pokNaKnj + i)->ISBN, (pokNaKnj + i)->broj_knjiga);
			}
		}
		else if(odg == 0)

			for (i = 0; i < a; i++) {
				printf("\n--{ %d. Knjiga }--\n\nIme knjige: %s\nIme autora: %s\nGodina izdanja: %d\n\nISBN knjige: %ld\n\nBroj knjiga: %d\n\n", i + 1, (pokNaKnj + i)->ime_knjige, (pokNaKnj + i)->autor, (pokNaKnj + i)->godina_izdanja, (pokNaKnj + i)->ISBN, (pokNaKnj + i)->broj_knjiga);
			}
	}

	fclose(fp);
	free(pokNaKnj);

	return 0;

}


void brojac(void) {

	FILE* br = NULL;
	br = fopen("binarna.bin", "rb+");
	if (br == NULL) {
		perror("Pogreska pri otvaranju datoteke!");
	}

	else {
		int brojac = 0;
		fread(&brojac, sizeof(int), 1, br);

		if (brojac == 0) {
			brojac = 1;
			fseek(br, 0, SEEK_SET);
			fwrite(&brojac, sizeof(int), 1, br);
		} //1.put
		else {
			brojac++;
			fseek(br, 0, SEEK_SET);
			fwrite(&brojac, sizeof(int), 1, br);
		} //vi�e puta
	}

	fclose(br);

	return 0;

}


void kreiranjeDatoteke() {
	FILE* kd = NULL;
	kd = fopen("binarna.bin", "wb");

	if (kd == NULL) {
		perror("Pogreska pri kreiranju datoteke!");
		return;
	}
	else {
		printf("--{ Datoteka kreirana }--\n");
		int b = 0;
		fwrite(&b, sizeof(int), 1, kd);
	}

	fclose(kd);

	return 0;
}


void provjeraDatoteke(void) {
	FILE* fp = fopen("binarna.bin", "rb");
	if (fp == NULL) {
		kreiranjeDatoteke();
	}
	else {
		printf("\n--{ Datoteka uspjesno otvorena! }--\n\n");
		fclose(fp);
	}

	return 0;

}


void provjera(void) {
	FILE* fp = fopen("binarna.bin", "rb");
	if (fp == NULL) {
		printf("Datoteka ne postoji. Kreiranje nove datoteke.\n");
		kreiranjeDatoteke();
	}
	else {
		printf("\nKnjige:\n\n");
		fclose(fp);
	}

	return 0;

}


void brisanje(void) {
	int d;
	d = remove("binarna.bin");
	if (d == 0) {
		printf("Datoteka je uspjesno obrisana.");
	}
	else {
		printf("Datoteka se nije obrisala.");
	}

	exit(EXIT_SUCCESS);

	return 0;

}

void pretraga(void) {
	int temp = 0, i;
	char ime[40];
	FILE* fp = NULL;
	KNJIGA* pokNaKnj = NULL;

	int x = 0;

	fp = fopen("binarna.bin", "rb");

	if (fp == NULL) {
		perror("Dogodila se greska.");
	}

	else {
		fread(&x, sizeof(int), 1, fp);
		pokNaKnj = (KNJIGA*)calloc(x, sizeof(KNJIGA));

		if (pokNaKnj == 1) 
			perror("Dogodila se greska.");

		fread(pokNaKnj, sizeof(KNJIGA), x, fp);

		printf("Ime trazene knjige: ");
		//scanf("%[^\n]s", ime);
		getchar();
		fgets(ime, 41, stdin);

		for (i = 0; i < x; i++) {
			if (strcmp((pokNaKnj + i)->ime_knjige, ime) == 0) {
				temp++;
				printf("\n--{ %d. Knjiga }--\n\nIme knjige: %s\nIme autora: %s\nGodina izdanja: %d\n\nISBN knjige: %ld\n\nBroj knjiga: %d\n", i + 1, (pokNaKnj + i)->ime_knjige, (pokNaKnj + i)->autor, (pokNaKnj + i)->godina_izdanja, (pokNaKnj + i)->ISBN, (pokNaKnj + i)->broj_knjiga);
			}
		}

		if (temp == 0) {
			printf("\nTrazena knjiga ne postoji!\n");
		}
	}

	fclose(fp);
	free(pokNaKnj);

	return 0;

}


void izlaz(void) {
	exit(EXIT_SUCCESS);

	return 0;

}


void sortiranje(KNJIGA*pokNaKnj, const int a) {

	int min = -1, i, j;

	for (i = 0; i < a - 1; i++) {
		min = i;
		for (j = i + 1; j < a; j++) {
			if ((pokNaKnj + j)->godina_izdanja < (pokNaKnj + min)->godina_izdanja) {
				min = j;
			}
		}

		zamjena((pokNaKnj + i), (pokNaKnj + min));
	}

	return 0;
}


void zamjena(KNJIGA* max, KNJIGA* min) {

	KNJIGA temp = *min;
	*min = *max;
	*max = temp;

	return 0;
}


