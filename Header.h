#pragma once
#ifndef HEADER_H
#define HEADER_H

typedef struct knjiga {
	char ime_knjige [40];
	char autor [40];
	int godina_izdanja;
	long int ISBN;
	int broj_knjiga;
	
} KNJIGA;

void izbornik(KNJIGA*);
void novaKnjiga(void);
void izlaz(void);
void pretraga(void);
void brisanje(void);
void ispis(void);
void brojac(void);
void kreiranjeDatoteke(void);
void provjeraDatoteke(void);
void provjera(void);
void sortiranje(KNJIGA*, const int);
void zamjena(KNJIGA*, KNJIGA*);


#endif // HEADER_H